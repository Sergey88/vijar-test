<?php

return [
    'workers_count' => 10,
    'age' => [14,120],
    'salary' => [2000,90000],
];
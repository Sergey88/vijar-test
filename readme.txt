Точка входа скрипта это index.php
Для работы нужно заинсталлить композер, используется psr-4.
Конфиг в файле config.php. Все файлы классов находятся в /src.

Структура:
/src
    /models - основные классы сущностей Worker и User, расширяются от базового Model
    /validators - каталог с возможными валидаторам
    Application.php - класс приложения. В методе run вся логика и вывод
    Config.php - класс для работы с конфином
config.php
index.php

Решил не реализовывать отдельно метод checkAge. Описал общую логику валидации полей, которая позволяет указывать валидатор в модели.


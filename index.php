<?php
require __DIR__ . '/vendor/autoload.php';

use app\Config;
use app\Application;

$config = new Config(__DIR__. '/config.php');

$app = new Application($config);
$app->run();
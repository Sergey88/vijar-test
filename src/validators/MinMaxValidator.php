<?php
namespace app\validators;

use app\validators\ValidatorInterface;

class MinMaxValidator implements ValidatorInterface 
{
    private $_error;

    public function validate($value, $params): bool 
    {
        if($value>=$params['min'] && $value<=$params['max']){
            return true;
        }
        $this->_error = 'Значение должно быть между '.$params['min'].' и '.$params['max'];
        return false;
    }
    
    public function getError(): string {
        return $this->_error;
    }
}
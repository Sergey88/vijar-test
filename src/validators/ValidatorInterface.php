<?php
namespace app\validators;

interface ValidatorInterface 
{
    public function validate($value, $params): bool;
    public function getError(): string;
}
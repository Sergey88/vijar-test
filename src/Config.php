<?php
namespace app;

class Config 
{
    private $_config;
    
    public function __construct($file)
    {
        $this->_config = require $file;
    }
    
    public function load(): array
    {
        return $this->_config;
    }
    
    public function get($opt)
    {
        return $this->_config[$opt];
    }
}
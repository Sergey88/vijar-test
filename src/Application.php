<?php
namespace app;

use app\models\Worker;
use app\models\User;

class Application 
{
    public static $config;
    
    public function __construct($config) 
    {
        self::$config = $config;
    }
    
    public function run()
    {
        echo '<pre>';
        $config = Application::$config;
        
        // Создаем 10 рандомных сотрудников
        for($i=0;$i<$config->get('workers_count');$i++){
            $data = [
                'age' => rand($config->get('age')[0],$config->get('age')[1]),
                'salary' => rand($config->get('salary')[0],$config->get('salary')[1]),
            ];
            $worker = new Worker($data);
            if ($worker->validate()) {
                $worker->save();
            }
        }
        // Вывод всех созданных обьектов сотрудников
        // print_r(Worker::getItems());
        
        // Создаем юзеров из половины сотрудников
        for($i=0;$i<$config->get('workers_count');$i=$i+2){
            $user = new User;
            $user->worker = Worker::getWorkerByName('name_'.$i);
            $user->save();
        }
        // Вывод всех созданных обьектов юзеров
        // print_r(User::getItems());
        
        // Получить id_user используя обькт Worker
        $id_user = Worker::getWorkerByName('name_4')->getUser()->id_user;
        
        $salaries = Worker::getSalaries();
        // print_r($salaries);
        
        // Максимальная зп
        $max_salary = Worker::getMaxSalary();
        // print_r($max_salary);
        
        // Сумма всей зп
        $total = Worker::totalSalary();
        // print_r($total);
        
        // Сумма зп юзеров
        $users_salary = Worker::getUsersSalary();
        // print_r($users_salary);
        
        // Сумма зп всех остальных
        $others_salary = $total - $users_salary;
        
        echo "<table>";
            echo "<tr>";
                echo "<th>Name</th>";
                echo "<th>Age</th>";
                echo "<th>Is User</th>";
                echo "<th>Salary</th>";
            echo "</tr>";
            foreach(Worker::$items as $worker){
                $user = $worker->getUser();
                $is_user = $worker->getUser() ? 'Да' : 'Нет';
                echo "<tr>";
                    echo "<td>".$worker->name."</td>";
                    echo "<td>".$worker->age."</td>";
                    echo "<td>".$is_user."</td>";
                    echo "<td>".$worker->salary."</td>";
                echo "</tr>";
            }            
        echo "</table>";
        
        echo "Сумма всей зп:           ".$total."</br>";
        echo "Сумма зп юзеров:         ".$users_salary."</br>";
        echo "Сумма зп всех остальных: ".$others_salary."</br>";
        echo "Максимальная зп:         ".$max_salary." - ".array_search($max_salary, $salaries)."</br>";
        
    }
}
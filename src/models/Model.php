<?php
namespace app\models;

class Model 
{
    protected $_attributes;
    protected $_errors = [];

    public function __construct(array $data = null) 
    {
        if($data){
            foreach ($data as $attr => $value) {
                $this->{$attr} = $value;
                $this->_attributes[$attr] = $value;
            }
        }
    }
    
    public function setAttributes(array $data): self
    {
        foreach($data as $attr=>$value){
            $this->{$attr} = $value;
            $this->_attributes[$attr] = $value;
        }
        
        return $this;
    }
    
    public function getAttributes(): array
    {
        return $this->_attributes;
    }
    
    public function validationRules(): array
    {
        return [];
    }
    
    public function validate()
    {
        $rules = $this->validationRules();
        if($rules){
            foreach($rules as $attr=>$params){
                $class_name = $params[0];
                $v = new $class_name();
                if(!$v->validate($this->{$attr}, $params[1])){
                    $this->_errors[] = $attr . ': ' . $v->getError();
                }
            }
        }
        if($this->getErrors()){
            return false;
        }
        return true;
    }
    
    public function getErrors(): array {
        return $this->_errors;
    }
    
    public function save()
    {
        
    }
    
   
        
}
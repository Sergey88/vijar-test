<?php
namespace app\models;

use app\models\Model;

class User extends Model 
{
    public $id_user;
    public $worker;
    public static $items = [];
    
    public function save() {
        $this->id_user = count($this->getItems()) + 1;
        self::$items[] = $this;
    }
    
    public static function getItems(){
        return self::$items;
    }
    
    public static function getWorker($worker){
        foreach(self::$items as $user){
            if($user->worker && $user->worker->name==$worker->name){
                return $user;
            }
        }
    }
}
<?php
namespace app\models;

use app\Application;
use app\models\Model;
use app\models\User;
use app\validators\MinMaxValidator;

class Worker extends Model 
{
    public $name;
    public $age;
    public $salary;
    public static $items = [];
    
    public function validationRules(): array
    {
        return [
            'age' => [
                MinMaxValidator::class,
                [
                    'min'=> Application::$config->get('age')[0],
                    'max'=> Application::$config->get('age')[1],
                ]
            ],
            'salary' => [
                MinMaxValidator::class,
                [
                    'min'=> Application::$config->get('salary')[0],
                    'max'=> Application::$config->get('salary')[1],
                ]
            ],
        ];
    }
    
    public function save() {
        $id = count($this->getItems()) + 1;
        $this->name = 'name_'.$id;
        self::$items[] = $this;
    }
    
    public static function getItems()
    {
        return self::$items;
    }
    
    public function getUser() {
        return User::getWorker($this);
    }
    
    public static function getWorkerByName($name){
        foreach(self::$items as $worker){
            if($worker->name==$name){
                return $worker;
            }
        }
    }
    
    public static function getSalaries(): array {
        $salaries = [];
        foreach(self::$items as $worker){
            $salaries[$worker->name] = $worker->salary;
        }
        return $salaries;
    }
    
    public static function getMaxSalary() {
        $salaries = self::getSalaries();
        return max($salaries);
    }
    
    public static function totalSalary() {
        $salaries = self::getSalaries();
        return array_sum($salaries);
    }
    
    public static function getUsersSalary() {
        $salary = 0;
        foreach(self::$items as $worker){
            $user = $worker->getUser();
            if($user){
                $salary += $worker->salary;
            }
        }
        return $salary;
    }
    
}